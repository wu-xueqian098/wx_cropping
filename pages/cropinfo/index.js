// pages/cropinfo/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    flag: true,
    itemData: [],
    showData: {
      imgurl: '',
      photo_id: '',
      collect: true,
    },




    
  },


 
  
  //跳转instructions
  navigateToInstructions(e) {
    let photo_id = e.target.dataset.photo_id;

    console.log("ta")
    console.log(photo_id)
    wx.navigateTo({
      url: '../instructions/index?photo_id='+photo_id,

    })
    
  },
  //返回
  back: function () {
    console.log('bb')
    wx.navigateBack({
      delta: 1
    })
  },
  off: function () {
    console.log('off')
    this.setData({
      flag: true,
    })
  },
  //查看
  show: function (e) {
    let photo_id = e.target.dataset.photo_id;
    let imgurl = e.target.dataset.imgurl;
    let collect = e.target.dataset.collect;
    let showimg = {
      imgurl: imgurl,
      photo_id: photo_id,
      collect: collect,
    }
    this.setData({
      flag: false,
      showData: showimg,
    })
  },
  //收藏
  show_heart: function (e) {
    let newshowData = this.data.showData
    newshowData.collect = !newshowData.collect
    let photo_id = newshowData.photo_id
    wx.request({
      method: "POST",
      url: 'https://www.bricke.cn:8020/collect_photo/',
      data: {
        photo_id: photo_id,
      },
      success(res) {}
    })

    this.setData({
      showData: newshowData
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.showLoading({
      title: '加载中',
    })
    this.getList();
    this.show_heart();
  },
 
  getList(e) {
    let that = this;
    wx.request({

      method: "POST",
      url: 'https://www.bricke.cn:8020/search_task_detil/',

      data: {
        taskid: that.options.taskid
      },

      success(res) {
        setTimeout(function () {
          wx.hideLoading()
        }, )

        console.log(res)
        that.setData({
          itemData: res.data
        })

      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})