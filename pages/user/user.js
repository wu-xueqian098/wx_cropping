// pages/user/user.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    is_login: false,
    itemData: []
  },
  login: function (e) {
    let that = this;
    wx.login({
      success(res) {
        if (res.code) {
          console.log(res.code)
          //发起网络请求
          wx.request({
            method: 'POST',
            url: 'https://www.bricke.cn:8020/wx_login',
            data: {
              jscode: res.code
            },
            success(res) {
              wx.setStorageSync('openid', res.data.openid)
              console.log(res.data)
              that.setData({
                is_login: true
              })
              that.getList()
            }
          })
        } else {
          console.log('登录失败！' + res.errMsg)
        }
      }
    })

  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    wx.showLoading({
      title: '加载中',
    })
    let openid = wx.getStorageSync('openid');
    if (openid) {
      this.setData({
        is_login: true
      })
      this.getList()
    } else {
      this.setData({
        is_login: false
      })
    }
  },
  //跳转cropinfo
  navigateToinfo(e) {
    var taskid = e.target.dataset.id;
    console.log("ta")
    console.log(taskid)
    wx.navigateTo({
      url: '../cropinfo/index?taskid='+taskid
    })
  },

  getList() {

    let that = this;
    wx.request({
      method: "POST",
      url: 'https://www.bricke.cn:8020/search_task/',
      data: {
        openid: wx.getStorageSync('openid'),
      }, success(res) {
        console.log(res)
        setTimeout(function () {
          wx.hideLoading()
        }, ) 
        that.setData({
         
          itemData:res.data
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})