// pages/instructions/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    photo_id: '',
    bookData: {
      guide_photo_id: '',
      imgurl: '',
    },

    distance: 0, // 手指移动的距离
    scale: 1, // 缩放比例
    baseWidth: '', // 图片实际宽度
    baseHeight: '', // 图片实际高度
    initWidth: '', // 图片默认显示宽度
    initHeight: '', // 图片默认显示高度
    scaleWidth: '', // 图片缩放后的宽度
    scaleHeight: '', // 图片缩放后的高度
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    this.getList(options);
    this.width = wx.getSystemInfoSync().windowWidth;
  },
  getList(e) {
    console.log(e)
    let that = this;
    wx.request({
      method: "POST",
      url: 'https://www.bricke.cn:8020/search_guide_photo/',
      data: {
        photo_id: e.photo_id,
      },
      success(res) {
        console.log(res)
        that.setData({
          bookData: res.data[0]
        })
      }
    })
  },
  imgload: function (e) {
    this.multiple = e.detail.width / this.width; // 计算原图和默认显示的倍数
    let height = this.multiple > 1 ? e.detail.height / this.multiple : e.detail.height; // 等比例计算出默认高度
    let width = this.multiple > 1 ? this.width : e.detail.width;
    this.setData({
      baseWidth: e.detail.width, // 获取图片实际宽度
      baseHeight: e.detail.height, // 获取图片实际高度
      initWidth: width,
      initHeight: height,
      scaleWidth: width,
      scaleHeight: height,
    })
  },
  /**
   * 双手指触发开始 计算开始触发两个手指坐标的距离
   */
  touchstartCallback: function (e) {
    console.log('11', e)
    // 单手指缩放开始，不做任何处理
    if (e.touches.length == 1) return;
    let distance = this.calcDistance(e.touches[0], e.touches[1]);
    console.log('222', this.data.distance)

    this.setData({
      'distance': distance,
    })
  },
  /**
   * 双手指移动   计算两个手指坐标和距离
   */
  touchmoveCallback: function (e) {
    console.log('111', e)
    // 单手指缩放不做任何操作
    if (e.touches.length == 1) return;
    let distance = this.calcDistance(e.touches[0], e.touches[1]);

    // 计算移动的过程中实际移动了多少的距离
    let distanceDiff = distance - this.data.distance;
    let newScale = this.data.scale + 0.005 * distanceDiff;
    if (newScale >= this.multiple && this.multiple > 2) { // 原图比较大情况
      newScale = this.multiple;
    } else if (this.multiple < 2 && newScale >= 2) { // 原图较小情况
      newScale = 2; // 最大2倍
    };
    // 最小缩放到0.3
    if (newScale <= 0.3) {
      newScale = 0.3;
    };

    let scaleWidth = newScale * this.data.initWidth;
    let scaleHeight = newScale * this.data.initHeight;
    this.setData({
      distance: distance,
      scale: newScale,
      scaleWidth: scaleWidth,
      scaleHeight: scaleHeight,
      diff: distanceDiff
    });
  },
  calcDistance(pos0, pos1) {
    let xMove = pos1.clientX - pos0.clientX;
    let yMove = pos1.clientY - pos0.clientY;
    return (Math.sqrt(xMove * xMove + yMove * yMove));
  },
  //保存到相册
  getBase64Url: function () {
    let that = this;
    var imgSrc = that.data.bookData.imgurl; //base64编码
    var save = wx.getFileSystemManager();
    var number = Math.random();
    save.writeFile({
      filePath: wx.env.USER_DATA_PATH + '/pic' + number + '.png',
      data: imgSrc,
      encoding: 'base64',
      success: res => {
        wx.saveImageToPhotosAlbum({
          filePath: wx.env.USER_DATA_PATH + '/pic' + number + '.png',
          success: function (res) {
            wx.showToast({
              title: '保存成功',
            })
          },
          fail: function (err) {
            console.log(err)
          }
        })
        console.log(res)
      },
      fail: err => {
        console.log(err)
      }
    })
  },
  store: function () {
    this.getBase64Url()

  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})