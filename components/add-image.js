import { selectPhoto } from '../utils/util'
Component({
  properties: {
		exts: {
			type: String,
			value: 'png、jpg、bmp、webp、ico'
		}
  },
  methods: {

    onChangeImage () {
      wx.showLoading({ title: '加载中' })

      selectPhoto((fileRes) => {
        const tempFiles = fileRes.tempFiles[0]
        const tempFilePath = tempFiles.tempFilePath || tempFiles.path
        wx.hideLoading()
        this.triggerEvent('syscImagePath', { value: tempFilePath })
      })
    }
  }
})
